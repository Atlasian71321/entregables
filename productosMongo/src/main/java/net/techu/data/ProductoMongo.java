package net.techu.data;


import org.springframework.data.mongodb.core.mapping.Document;

@Document("productos")
public class ProductoMongo {

    public String id;
    public String nombre;
    public Double precio;
    public String color;

    public ProductoMongo(){
        //Constructor
    }

    public ProductoMongo(String nombre, Double precio, String color) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
    }

    @Override
    public String toString() {
        return "ProductoMongo{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", precio='" + precio + '\'' +
                '}';
    }

}


