package com.clientes.clientes.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("clientesTarea1LF")
public class ClienteMongo {

    public String id;
    public String nombre;
    public String apellidos;
    public String ciudad;

    public ClienteMongo() {
    }

    public ClienteMongo(String nombre, String apellidos, String ciudad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return String.format("Cliente [id=%s, nombre=%s, apellidos=%s, ciudad=%s]", id, nombre, apellidos, ciudad);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
