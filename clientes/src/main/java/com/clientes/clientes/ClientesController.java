


package com.clientes.clientes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.clientes.clientes.data.ClienteMongo;
import com.clientes.clientes.data.ProductRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientesController {

    @Autowired
    private ProductRepository repository;

    // Get lista de clientes
    @GetMapping(value = "/v1/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListadoClientes()
    {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<List<ClienteMongo>>(lista, HttpStatus.OK);
    }

    //Get cliente
    @GetMapping(value = "/v1/clientes/{id}", produces = "application/json")
    public ResponseEntity<ClienteMongo> obtenerCliente(@PathVariable String id)
    {
        ClienteMongo clienteMongo = null;
        try {
            clienteMongo = repository.findById(id).get();
            return new ResponseEntity<ClienteMongo>(clienteMongo, HttpStatus.OK);

        }
        catch(Exception exception) {
            return new ResponseEntity<ClienteMongo>(clienteMongo, HttpStatus.NOT_FOUND);
        }
    }

    //Post
    @PostMapping(value="/v1/clientes", produces = "application/json")
    public ResponseEntity<String> addCliente(@RequestBody ClienteMongo clienteMongo)
    {
        ClienteMongo resultado = repository.insert(clienteMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //Put
    @PutMapping(value="/v1/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable String id, @RequestBody ClienteMongo clienteMongo){

        if (repository.findById(id).isPresent()){
            ClienteMongo resultado = repository.findById(id).get();
            resultado.setApellidos(clienteMongo.getApellidos());
            resultado.setNombre(clienteMongo.getNombre());
            resultado.setCiudad(clienteMongo.getCiudad());
            repository.save(resultado);
            return new ResponseEntity<>("Cliente modificado", HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>("Cliente no modificado", HttpStatus.NOT_FOUND);
    }

    //Delete
    @DeleteMapping(value="/v1/clientes/{id}", produces = "application/json")
    public ResponseEntity<String> deleteCliente (@PathVariable String id){
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
            return new ResponseEntity<String>("Cliente borrado", HttpStatus.NO_CONTENT);
        }else {
            return new ResponseEntity<String>("Cliente no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    //Patch
    @PatchMapping(value="/v1/clientes/{id}", produces = "application/json")
    public ResponseEntity<String> patchCliente (@RequestBody ClienteMongo clienteMongo, @PathVariable String id){


            if (repository.findById(id).isPresent()){
                ClienteMongo resultado = repository.findById(id).get();
                resultado.setCiudad(clienteMongo.ciudad);
                repository.save(resultado);
                return new ResponseEntity<>("Cliente modificado", HttpStatus.NO_CONTENT);
            }
            else {
                return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
            }
    }
}
