package com.clientes.clientes;

public class Clientes {

    private int id;
    private String nombre;
    private String apellidos;
    private String ciudad;


    public Clientes(int id, String nombre, String apellidos, String ciudad) {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellidos(apellidos);
        this.setCiudad(ciudad);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

}
